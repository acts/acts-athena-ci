#!/usr/bin/env python3
import argparse
from pathlib import Path
import re
import subprocess
import tempfile
import os


def run_idpvm(base: Path, file: Path, infix: str, dry_run: bool):
    output_file = base / (file.stem + infix + ".IDPVM.root")
    print("Run IDPVM on", file, "->", output_file)
    if not dry_run:
        with tempfile.TemporaryDirectory() as tmpdir:
            subprocess.run(
                ["runIDPVM.py", "--filesInput", file, "--outputFile", output_file],
                cwd=tmpdir,
                check=True,
            )


def main():
    p = argparse.ArgumentParser()
    p.add_argument("workflow_run_dir", help="workflow file", type=Path)
    p.add_argument("--dry-run", "-S", action="store_true", help="Dry run")

    args = p.parse_args()

    assert args.workflow_run_dir.is_dir(), "Workflow run dir not found"
    args.workflow_run_dir = args.workflow_run_dir.resolve()

    logfiles = []
    monitored_file = None
    for root, _, files in os.walk(args.workflow_run_dir):
        root = Path(root)
        for file in files:
            full_file = root / file

            if file.endswith(".log"):
                logfiles.append(full_file)
            if file == "myAOD.pool.root":
                monitored_file = full_file

    if len(logfiles) == 0:
        raise FileNotFoundError("Logfile not found")
    if monitored_file is None:
        raise FileNotFoundError("Monitored file not found")

    reference_file = None
    for logfile in logfiles:
        log = logfile.read_text()

        m = re.search(r".*Reading the reference file from location (.+AOD.+)", log)

        if m is not None:
            reference_file = Path(m.group(1))
            if not args.dry_run:  # doesn't need to be readable if dry run
                assert reference_file.is_file(), "Reference file not found"
            break

    if reference_file is None:
        raise FileNotFoundError("Reference file not found")

    run_idpvm(args.workflow_run_dir, monitored_file, ".monitored", dry_run=args.dry_run)
    run_idpvm(args.workflow_run_dir, reference_file, ".reference", dry_run=args.dry_run)


if __name__ == "__main__":
    main()
