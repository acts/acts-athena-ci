if [[ ! -d "${ATHENA_SOURCE}" ]]; then
  echo "No Athena source directory specified"
  exit 1
fi

export NIGHTLY_DIR=/cvmfs/atlas-nightlies.cern.ch/repo/sw/${ATHENA_RELEASE}_Athena_${LCG_PLATFORM}

if [ ! -d "$NIGHTLY_DIR" ]; then
    echo "No nightly directory found for ${ATHENA_RELEASE} on ${LCG_PLATFORM}"
    echo "This is most likely a CVMFS issue"
    exit 1
fi


export NIGHTLY_NAME=$(ls ${NIGHTLY_DIR} | grep -e "^[0-9]\+" | tail -1)
export NIGHTLY_DATE=$(echo "${NIGHTLY_NAME}" | tail -1 | sed -E 's/(.*)T([0-9]{2})([0-9]{2})/\1T\2:\3:00/g')

pushd "${ATHENA_SOURCE}" > /dev/null
export NIGHTLY_COMMIT=$(git log --until="$NIGHTLY_DATE" -1 --first-parent --format=format:%H)
popd > /dev/null
