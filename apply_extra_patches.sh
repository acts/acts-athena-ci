#!/bin/bash
set -e
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}"   )" &> /dev/null && pwd   )

echo "Applying extra patches"

# set -x
for f in "$SCRIPT_DIR/patches/"*.patch; do
    base=$(basename "$f")
    echo "---- Applying: $base"
    git am -3 "$f"
done
