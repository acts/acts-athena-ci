# Athena build with custom ACTS version

To make development of ACTS-side code easier, it helps to have a local build of
ACTS and build Athena packages on top of it.  With a bit of environment
variable hacking this is possible.

## CI builds and development branch
The CI on this repository runs like this:
- Sets up LCG compatible with Athena `main` and compiles ACTS `main` with it
- Sets up and checks out the most recent Athena `main--ACTS` nightly
- Merges in all MRs labeled `nightly:ACTS` targeting Athena `main`
- Builds the packages listed in `package_filters.txt` on with that new ACTS version from `main`
- Runs a number of test scripts and tests from Athena `main` that use ACTS

When a breaking change is made to ACTS main that breaks the build or one of the
workflows, please get in touch, and we'll add the changes needed to the
`canary` branch. Once that's done, the CI should go green again, indicating
that ACTS `main` is compatible with the latest Athena nightly.

## Using this CI setup

### Compile ACTS + Athena
The primary entry point is a single script called `test_athena.sh`, located in the root of this repository.
When run it will:
1. Clone ACTS in `$PWD/acts` if you don't have it already
2. Clone Athena `$PWD/athena` if you don't have it already
   If you do have an Athena cloned, it will ask you if you want to *keep* or *wipe* it
  1. *keep* will keep your current Athena as is
  2. *wipe* will (try to) reset your Athena clone to be consistent with the
     combined source mentioned in the previous section. This can sometimes fail
     due to the supplied patches being out of sync. If that happens, please get in
     touch!
3. Build ACTS using a combined build environment from AtlasExternals and LCG.
   The build (by default) happens in `/tmp/$USER/ath_build`, and this directory
   be **fully overwritten** each time you call `test_athena.sh`.
4. Configure an environment for building Athena with the ACTS version built in step 3
5. Drop you into a shell (bash) in that folder with the Athena. From here, you can run
   ```bash
   cmake --build athena-build
   ```
   to build Athena.

### Rebuilding ACTS

If you want to make changes to ACTS and update the compiled ACTS you can
1. In a **new shell** `cd` to `/tmp/$USER/ath_build`
2. Run the script called `acts_build.sh`. This will launch a new shell with the combined build environment for ACTS discussed above. Here, you can run
   ```bash
   cmake --build acts-build && cmake --install acts-build
   ```
   to build and install the updated ACTS
3. Rerun the Athena build command from above to update your Athena build.

### Running jobs in combined setup

If you want to run an Athena job with the combined compiled release:

1. In a **new shell** `cd` to `/tmp/$USER/ath_build`
2. Run `asetup --restore` to set up the same Athena release as the build was done with
3. Run `source athena-build/*/setup.sh` (where `*` is supposed to resolve to your platform specific build directory, should happen automatically)
4. Run you job
