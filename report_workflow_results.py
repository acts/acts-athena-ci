#!/usr/bin/env python3
import asyncio
from typing import Dict, Any, Optional
import re

import typer
import aiohttp
import gidgetlab.aiohttp
import gidgethub.aiohttp
import rich.console
import rich.panel
import rich.markdown
import rich.emoji
import rich.align

console = rich.console.Console()

from gidgethub.abc import GitHubAPI
from gidgetlab.abc import GitLabAPI

ATHENA_BUILD_FAILURE_LABEL = "Breaks Athena build"
ATHENA_TEST_FAILURE_LABEL = "Fails Athena tests"

app = typer.Typer(pretty_exceptions_enable=False)


@app.command()
def main(
    gitlab_pipeline_id: int = typer.Argument(envvar="CI_PIPELINE_ID"),
    gitlab_token: str = typer.Option(
        ..., envvar=["REPORT_GITLAB_TOKEN", "CI_JOB_TOKEN"]
    ),
    gitlab_url: str = typer.Option(
        "https://gitlab.cern.ch", envvar="REPORT_GITLAB_URL"
    ),
    gitlab_project: str = typer.Option(..., envvar=["REPORT_GITLAB_PROJECT"]),
    github_token: str = typer.Option(..., envvar="REPORT_GITHUB_TOKEN"),
    github_project: str = typer.Option(
        "acts-project/acts", envvar="REPORT_GITHUB_PROJECT"
    ),
    github_pull_request: Optional[int] = typer.Option(None, envvar="SOURCE_PULL"),
    trigger_commit: Optional[str] = typer.Option(None, envvar="SOURCE_SHA"),
    dry_run: bool = typer.Option(False, "--dry-run", "-n"),
):
    async def wrap():
        async with aiohttp.ClientSession() as session:
            gl = gidgetlab.aiohttp.GitLabAPI(
                session, requester="acts", access_token=gitlab_token, url=gitlab_url
            )
            gh = gidgethub.aiohttp.GitHubAPI(
                session, requester="acts", oauth_token=github_token
            )

            project = gitlab_project.replace("/", "%2F")
            await report(
                gh=gh,
                gl=gl,
                gl_project=project,
                gh_project=github_project,
                gitlab_pipeline_id=gitlab_pipeline_id,
                pull_request_number=github_pull_request,
                trigger_commit=trigger_commit,
                dry_run=dry_run,
            )

    asyncio.run(wrap())


async def find_comment(gh: GitHubAPI, pull_request: Dict[str, Any]) -> str | None:
    user = await gh.getitem("/user")
    comment = None
    async for c in gh.getiter(pull_request["comments_url"]):
        if c["user"]["id"] == user["id"]:
            comment = c["url"]
            break

    return comment


async def create_or_update_comment(
    gh: GitHubAPI, pull_request: Dict[str, Any], comment: str, dry_run: bool
):
    console.print(
        f"Posting to {pull_request['base']['repo']['full_name']}#{pull_request['number']}"
    )

    comment_url = await find_comment(gh=gh, pull_request=pull_request)

    if comment_url is None:
        console.print("No comment found, will create new comment")
    else:
        console.print("Found comment at ", comment_url)

    if dry_run:
        console.print(
            rich.panel.Panel(
                rich.align.Align("Dry run requested. Not posting!", align="center"),
                style="red bold",
            )
        )
    else:
        if comment_url is None:
            console.print("Posting new comment to ", pull_request["comments_url"])
            await gh.post(pull_request["comments_url"], data={"body": comment})
        else:
            console.print("Updating comment at ", comment_url)
            await gh.patch(comment_url, data={"body": comment})

    console.print(
        rich.panel.Panel(
            rich.markdown.Markdown(rich.emoji.Emoji.replace(comment)),
            title="Comment",
        )
    )


async def report(
    gh: GitHubAPI,
    gl: GitLabAPI,
    gl_project: str,
    gh_project: str,
    gitlab_pipeline_id: int,
    dry_run: bool,
    pull_request_number: int | None = None,
    trigger_commit: str | None = None,
):
    pipeline = await gl.getitem(
        f"/projects/{gl_project}/pipelines/{gitlab_pipeline_id}"
    )
    jobs = [
        j
        async for j in gl.getiter(
            f"/projects/{gl_project}/pipelines/{pipeline['id']}/jobs"
        )
    ]

    build_job = None
    for job in jobs:
        if job["name"] == "build":
            build_job = job
            break
    assert build_job is not None, "Build job not found!"

    if pull_request_number is None:
        trace = await gl.getitem(f"/projects/{gl_project}/jobs/{build_job['id']}/trace")

        m = re.search(r"ACTS is at:\n-+\ncommit (\S*)$", trace, re.MULTILINE)

        acts_commit_sha = m.group(1)
        console.print(f"ACTS commit: [bold]{acts_commit_sha}[/bold]")
        assert m is not None, "ACTS commit version not found"

        acts_commit = await gh.getitem(f"/repos/{gh_project}/commits/{acts_commit_sha}")
        acts_commit_message = acts_commit["commit"]["message"]

        console.print(
            rich.panel.Panel(
                acts_commit_message,
                title="ACTS commit message",
            )
        )

        short_message: str = acts_commit_message.split("\n")[0]
        m = re.match(r".*\(#(\d+)\)$", short_message)
        assert m is not None, "Unable to extract pull request number"

        pull_request_number = int(m.group(1))

    pr_url = f"/repos/{gh_project}/pulls/{pull_request_number}"

    pull_request = await gh.getitem(pr_url)
    pr_web_url = pull_request["html_url"]

    console.print(
        rich.panel.Panel(
            f"[bold]{gh_project}#{pull_request_number}[/bold] ({pr_web_url})",
            title="Pull request",
        )
    )

    labels = set([l["name"] for l in pull_request["labels"]])
    labels -= {ATHENA_BUILD_FAILURE_LABEL, ATHENA_TEST_FAILURE_LABEL}

    trigger_commit_str = f" [{trigger_commit}]" if trigger_commit is not None else ""

    if build_job["status"] == "failed":
        console.print("[red][bold]Build job failed[/bold][/red]")
        comment = (
            f"## :red_circle: Athena integration test results{trigger_commit_str}\n"
            + "### Build job with this PR failed!\n"
            + f"Please investigate the [build job]({build_job['web_url']}) "
            + f"for the [pipeline]({pipeline['web_url']})!"
        )
        await create_or_update_comment(
            gh=gh, pull_request=pull_request, comment=comment, dry_run=dry_run
        )
        labels.add(ATHENA_BUILD_FAILURE_LABEL)
        if not dry_run:
            await gh.post(
                f"/repos/{gh_project}/issues/{pull_request['number']}/labels",
                data={"labels": list(labels)},
            )
        return

    workflow_jobs = [j for j in jobs if j["stage"] != "build"]

    status, comment = await analyze_test_jobs(
        workflow_jobs,
        gl=gl,
        pipeline_url=pipeline["web_url"],
        trigger_commit=trigger_commit,
        gl_project=gl_project,
    )

    await create_or_update_comment(
        gh=gh, pull_request=pull_request, comment=comment, dry_run=dry_run
    )

    if not status:
        labels.add(ATHENA_TEST_FAILURE_LABEL)
        if not dry_run:
            await gh.post(
                f"/repos/{gh_project}/issues/{pull_request['number']}/labels",
                data={"labels": list(labels)},
            )


async def analyze_test_jobs(
    test_jobs,
    gl: GitLabAPI,
    pipeline_url: str,
    trigger_commit: str | None,
    gl_project: str,
) -> tuple[bool, str]:
    status = True
    comment = ["", "|status|job|report|", "|------|---|------|"]

    traces = await asyncio.gather(
        *[
            gl.getitem(f"/projects/{gl_project}/jobs/{job['id']}/trace")
            for job in test_jobs
        ]
    )

    for job, trace in zip(test_jobs, traces):
        emoji = ":green_circle:"

        if job["status"] == "failed":
            if job["allow_failure"]:
                emoji = ":yellow_circle:"
            else:
                status = False
                emoji = ":red_circle:"

        extra = "|"
        if m := re.search(r"PUBLIC_URL: (.+)", trace, re.MULTILINE):
            extra = f" | [:paperclip:]({m.group(1)})"

        name = job["name"].replace("[", "").replace("]", "")
        comment += [f"|{emoji} | [{name}]({job['web_url']}){extra}|"]

    comment += [""]

    trigger_commit_str = f" [{trigger_commit}]" if trigger_commit is not None else ""

    if status == True:
        comment = [
            f"## :white_check_mark: Athena integration test results{trigger_commit_str}\n"
            "### :white_check_mark: All tests successful"
        ] + comment
    else:
        comment = [
            f"## :red_circle: Athena integration test results{trigger_commit_str}\n"
            "### :red_circle: Some tests have failed!",
            f"Please investigate the [pipeline]({pipeline_url})!",
        ] + comment

    return status, "\n".join(comment)


if "__main__" == __name__:
    app()
