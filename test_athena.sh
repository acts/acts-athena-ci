#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase

export LCG_PLATFORM=x86_64-el9-gcc13-opt
if [ `uname -m` == "aarch64" ]; then
  export LCG_PLATFORM=aarch64-el9-gcc13-opt
fi

export ACTS_GIT_REPO=https://github.com/acts-project/acts.git
export ACTS_REF=main

export ATHENA_GIT_REPO=https://gitlab.cern.ch/atlas/athena.git
export ATHENA_RELEASE=main--ACTS
export ATHENA_REF=main

_pwd=$PWD

export ATHENA_SOURCE=$PWD/athena
export ACTS_SOURCE=$PWD/acts

tmp=/tmp/$(whoami)/ath_build
rm -rf $tmp
mkdir -p $tmp
pushd $tmp
ln -s $_pwd/package_filters_hack.txt package_filters.txt
$_pwd/athena_build.sh
popd
