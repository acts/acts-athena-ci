#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}"  )" &> /dev/null && pwd  )

set -e

source $SCRIPT_DIR/helpers.sh

echo "------------- Initial configuration: ------------------"
echo "ATLAS_LOCAL_ROOT_BASE: ${ATLAS_LOCAL_ROOT_BASE}"
echo "ATHENA_SOURCE: ${ATHENA_SOURCE}"
echo "ACTS_SOURCE: ${ACTS_SOURCE}"
echo "LCG_PLATFORM: ${LCG_PLATFORM}"
echo "-------------------------------------------------------"

source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh || true
asetup none,gcc13 || true
lsetup cmake || true
lsetup git || true
NINJA=/cvmfs/sft.cern.ch/lcg/releases/ninja/1.10.0-d608d/${LCG_PLATFORM}/bin/ninja

group "Setup ACTS"


EXTRA_FLAGS=""
if [ -t 1  ]; then
    EXTRA_FLAGS="-fdiagnostics-color=always"
fi

if [[ ! -d "${ACTS_SOURCE}"  ]]; then
    group "Cloning ACTS: ${ACTS_GIT_REPO} @ ${ACTS_REF}"
    git clone ${ACTS_GIT_REPO} -b ${ACTS_REF} ${ACTS_SOURCE}

    if [ ! -z "$REVERT_SHAS" ]; then
        pushd "${ACTS_SOURCE}"
        echo "$REVERT_SHAS" | sed -n 1'p' | tr ',' '\n' | while read sha; do

        echo "---- Reverting ${sha::9} ----"
        git --no-pager log -1 $sha
        echo "----------------"
        git revert $sha
        done
        popd
    fi

else
    echo "${ACTS_SOURCE} already exists"
    echo "Assuming you know what you're doing"
fi

if [ ! -z "$SOURCE_SHA" ]; then
    echo "Checking out explicit commit ${SOURCE_SHA}"
    pushd "${ACTS_SOURCE}"
    git checkout $SOURCE_SHA
    popd
fi

echo
echo "ACTS is at:"
echo "-----------"
pushd "${ACTS_SOURCE}" > /dev/null
git --no-pager log -1
popd > /dev/null
echo "-----------"
echo


if [ -n "${BUILD_GEOMODEL}" ]; then
    group "Setup GeoModel"

    if [[ ! -d "${GEOMODEL_SOURCE}" ]]; then
        echo "Cloning GeoModel: ${GEOMODEL_GIT_REPO} @ ${GEOMODEL_REF}"
        git clone ${GEOMODEL_GIT_REPO} -b ${GEOMODEL_REF} ${GEOMODEL_SOURCE}
    else
        echo "${GEOMODEL_SOURCE} already exists"
        echo "Assuming you know what you're doing"
    fi

    echo 
    echo "GeoModel is at:"
    echo "--------------"
    pushd "${GEOMODEL_SOURCE}" > /dev/null
    git --no-pager log -1
    popd > /dev/null
    echo "--------------"
    echo
fi

LCG_VERSION_NUMBER=$(sed -n "s/.*LCG_VERSION_NUMBER=\(\S*\)$/\1/p" ${ATHENA_SOURCE}/Projects/Athena/build_externals.sh)
LCG_VERSION_POSTFIX=$(sed -n "s/.*LCG_VERSION_POSTFIX=\"\(\S*\)\"$/\1/p" ${ATHENA_SOURCE}/Projects/Athena/build_externals.sh)
LCG_RELEASE="LCG_${LCG_VERSION_NUMBER}${LCG_VERSION_POSTFIX}"

echo "LCG_RELEASE: ${LCG_RELEASE}"

source $SCRIPT_DIR/find_nightly_commit.sh

athena_cmake_prefix=$(bash -c "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh 2>&1 > /dev/null; asetup Athena,${ATHENA_RELEASE},${NIGHTLY_NAME} 2>&1 > /dev/null; echo \$CMAKE_PREFIX_PATH")

lsetup "views ${LCG_RELEASE} ${LCG_PLATFORM}" || true
CCACHE=$(command -v ccache)


echo "#!/bin/bash" > acts_build.sh
echo "set -e" >> acts_build.sh
echo "source \${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh || true" >> acts_build.sh
echo "lsetup \"views ${LCG_RELEASE} ${LCG_PLATFORM}\" || true" >> acts_build.sh
echo "lcg_cmake_prefix_path=\$CMAKE_PREFIX_PATH" >> acts_build.sh
echo "asetup Athena,${ATHENA_RELEASE},${NIGHTLY_NAME} || true" >> acts_build.sh
echo "export CMAKE_PREFIX_PATH=\"\${lcg_cmake_prefix_path}:\$CMAKE_PREFIX_PATH\"" >> acts_build.sh
echo "echo \"ACTS build setup ready, call:\"" >> acts_build.sh
echo "echo \"- cmake --build acts-build\" to build" >> acts_build.sh
echo "echo \"- cmake --install acts-build\" to install" >> acts_build.sh
echo "bash" >> acts_build.sh
chmod +x acts_build.sh

setup_file=setup_combined.sh
echo "#!/bin/bash" > $setup_file
echo "source \${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh || true" >> $setup_file
echo "pushd $PWD > /dev/null" >> $setup_file
echo "asetup --restore || true" >> $setup_file
echo "popd > /dev/null" >> $setup_file

echo "if [[ -d \"$PWD/athena-build\" ]]; then" >> $setup_file
echo "  echo \"-> Setting up build directory $PWD/athena-build\"" >> $setup_file
echo "  source $PWD/athena-build/*/setup.sh" >> $setup_file
echo "fi" >> $setup_file

echo "export LD_LIBRARY_PATH=$PWD/acts-install/lib64:\$LD_LIBRARY_PATH" >> $setup_file
if [ -n "${BUILD_GEOMODEL}" ]; then
  echo "export LD_LIBRARY_PATH=$PWD/geomodel-install/lib64:\$LD_LIBRARY_PATH" >> $setup_file
fi

echo "source $PWD/$setup_file" >> run.sh
echo "bash" >> run.sh
chmod +x run.sh


if [ -n "${BUILD_GEOMODEL}" ]; then
    group "Configure GeoModel"

    if [ -d "geomodel-build" ]; then
        echo "Cleaning up existing build directory"
        rm -rf geomodel-build
    fi

    cmake -S "${GEOMODEL_SOURCE}" -B geomodel-build \
        -GNinja \
        -DCMAKE_MAKE_PROGRAM="$NINJA" \
        -DCMAKE_CXX_FLAGS="$EXTRA_FLAGS" \
        -DCMAKE_CXX_STANDARD=20 \
        -DCMAKE_INSTALL_PREFIX=$PWD/geomodel-install \
        -DCMAKE_CXX_COMPILER_LAUNCHER=$CCACHE \
        -DGEOMODEL_BUILD_TOOLS:BOOL=TRUE
    group "Build GeoModel"
    $CCACHE -z
    cmake --build geomodel-build -- -j${BUILD_JOBS}
    $CCACHE -s

    group "Install GeoModel"
    cmake --install geomodel-build > geomodel_install.log

fi

$CCACHE -z

group "Configure ACTS"

acts_install_dir=$PWD/acts-install

# Recover ATLAS cmake prefix path so we can find LCG packages
if [ -n "${BUILD_GEOMODEL}" ]; then
  # With GeoModel: GeoModel CMake prefix comes first
  export CMAKE_PREFIX_PATH="${PWD}/geomodel-install:${athena_cmake_prefix}:$CMAKE_PREFIX_PATH"
else
  # Without GeoModel: Athena CMake prefix comes first
  export CMAKE_PREFIX_PATH="${athena_cmake_prefix}:$CMAKE_PREFIX_PATH"
fi
echo $CMAKE_PREFIX_PATH

if [ -d "acts-build" ]; then
  echo "Cleaning up existing build directory"
  rm -rf acts-build
fi

cmake -S "${ACTS_SOURCE}" -B acts-build \
  -GNinja \
  -DCMAKE_MAKE_PROGRAM="$NINJA" \
  -DCMAKE_CXX_FLAGS="$EXTRA_FLAGS" \
  -DCMAKE_INSTALL_PREFIX=$acts_install_dir \
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
  -DACTS_USE_SYSTEM_NLOHMANN_JSON:BOOL=ON \
  -DCMAKE_CXX_COMPILER_LAUNCHER=$CCACHE \
  -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
  -DACTS_BUILD_PLUGIN_JSON:BOOL=ON \
  -DACTS_BUILD_PLUGIN_GEOMODEL:BOOL=ON \
  -DACTS_BUILD_PLUGIN_ACTSVG:BOOL=ON \
  -DACTS_BUILD_FATRAS:BOOL=ON

group "Build ACTS"

cmake --build acts-build -- -j${BUILD_JOBS}
$CCACHE -s

group "Install ACTS"
cmake --install acts-build > acts_install.log

group "Setup Athena"

n=10
pushd ${ATHENA_SOURCE} > /dev/null
echo
echo "Last $n commits:"
echo "--------------"
git --no-pager log -n$n --pretty=short
echo "--------------"
popd > /dev/null
echo

$CCACHE -z

asetup Athena,${ATHENA_RELEASE},${NIGHTLY_NAME} || true

export CMAKE_PREFIX_PATH="${acts_install_dir}:$CMAKE_PREFIX_PATH"

if [ -n "${BUILD_GEOMODEL}" ]; then
  export CMAKE_PREFIX_PATH="${PWD}/geomodel-install:$CMAKE_PREFIX_PATH"
fi


group "Configure Athena"

if [ -d "athena-build" ]; then
  echo "Cleaning up existing build directory"
  rm -rf athena-build
fi

mkdir athena-build
install_dir=$PWD/athena-install
pushd athena-build
cmake "$ATHENA_SOURCE/Projects/WorkDir" \
  -GNinja \
  -DCMAKE_MAKE_PROGRAM="$NINJA" \
  -DCMAKE_CXX_FLAGS="$EXTRA_FLAGS" \
  -DATLAS_PACKAGE_FILTER_FILE=$PWD/../package_filters.txt \
  -DCMAKE_CXX_COMPILER_LAUNCHER=$CCACHE \
  -DCMAKE_INSTALL_PREFIX=$install_dir
popd

echo "Patching env_setup.sh to pick up correct ACTS shared libs"

echo "" >> athena-build/*/env_setup.sh
echo "# ACTS hack build injected library path:" >> athena-build/*/env_setup.sh
echo "export LD_LIBRARY_PATH=\"${acts_install_dir}/lib64:\$LD_LIBRARY_PATH\"" >> athena-build/*/env_setup.sh

group "Build Athena"
