asetup Athena,${ATHENA_RELEASE},latest || true
git config --global user.name gitlab
git config --global user.email cijob@example.com
source current_nightly_tag.sh
./prepare_athena.sh
source athena-build/x*/setup.sh
export LD_LIBRARY_PATH=$PWD/acts-install/lib64:$LD_LIBRARY_PATH
