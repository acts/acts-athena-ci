#!/usr/bin/env python3

import argparse
import os
import platform
import shutil
from pathlib import Path
import sys
import multiprocessing
import enum
import dataclasses
import subprocess
from typing import List
import json
import re
import tempfile

description = """
Build ACTS and Athena together locally. This script sets up the necessary environment 
and handles the build configuration for both projects.

The script will:
1. Clone and build ACTS from a specified repository/branch
2. Clone and build Athena against a specified release
3. Set up the correct LCG platform and environment variables

The ACTS build environment is set up to take dependencies from the ATLAS Externals 
where necessary, and from the LCG version matching the Athena release otherwise.
"""

DEFAULT_ACTS_REPO = "https://github.com/acts-project/acts.git"
DEFAULT_ACTS_REF = "main"
DEFAULT_ACTS_SOURCE = Path.cwd() / "acts"

DEFAULT_ATHENA_REPO = "https://gitlab.cern.ch/atlas/athena.git"
DEFAULT_ATHENA_REF = "main"
DEFAULT_ATHENA_RELEASE = "main"
DEFAULT_ATHENA_SOURCE = Path.cwd() / "athena"

DEFAULT_GEOMMODEL_REPO = "https://gitlab.cern.ch/GeoModelDev/GeoModel.git"
DEFAULT_GEOMODEL_SOURCE = Path.cwd() / "geomodel"
DEFAULT_GEOMODEL_REF = "main"


def get_arg_parser() -> argparse.ArgumentParser:
    p = argparse.ArgumentParser(
        description=description, formatter_class=argparse.RawDescriptionHelpFormatter
    )

    p.add_argument(
        "--acts-repo",
        type=str,
        default=DEFAULT_ACTS_REPO,
        help="ACTS git repository",
    )

    p.add_argument(
        "--acts-ref",
        type=str,
        default=DEFAULT_ACTS_REF,
        help="ACTS git reference. Set to 'auto' to let the script determine the version based on the Athena release",
    )

    p.add_argument(
        "--acts-source",
        type=Path,
        default=DEFAULT_ACTS_SOURCE,
        help="ACTS source directory",
    )

    p.add_argument(
        "--athena-repo",
        type=str,
        default=DEFAULT_ATHENA_REPO,
        help="Athena git repository url",
    )

    p.add_argument(
        "--athena-ref",
        type=str,
        default=DEFAULT_ATHENA_REF,
        help="Athena git reference",
    )

    p.add_argument(
        "--athena-source",
        type=Path,
        default=DEFAULT_ATHENA_SOURCE,
        help="Athena source directory",
    )

    p.add_argument(
        "--athena-release",
        type=str,
        default=DEFAULT_ATHENA_RELEASE,
        help="Athena release to build the Athena clone against",
    )

    p.add_argument(
        "--geomodel-repo",
        type=str,
        default=DEFAULT_GEOMMODEL_REPO,
        help="GeoModel git repository",
    )

    p.add_argument(
        "--geomodel-ref",
        type=str,
        default=DEFAULT_GEOMODEL_REF,
        help="GeoModel git reference Set to 'auto' to let the script determine the version based on the Athena release",
    )

    p.add_argument(
        "--geomodel-source",
        type=Path,
        default=DEFAULT_GEOMODEL_SOURCE,
        help="GeoModel source directory",
    )

    p.add_argument(
        "--build-geomodel",
        action="store_true",
        help="Include the GeomModel package in the build. If this is disabled, ACTS is built against the Externals version of the GeomModel package.",
    )

    p.add_argument(
        "--add-compile-commands",
        action="store_true",
        help="Add compile_commands.json to the build directory",
    )

    p.add_argument(
        "--directory",
        type=Path,
        help="Directory to build in. Defaults to a temporary directory based on the current user name.",
    )

    p.add_argument(
        "--interactive",
        "-i",
        action="store_true",
        help="Drop into a shell after the build is configured. This is useful for debugging.",
    )

    p.add_argument(
        "--no-athena-acts-canary",
        action="store_true",
        help="Do not assemble the Athena source with the ACTS canary. Uses the latest nightly for the Athena ref given directly as-is.",
    )

    p.add_argument(
        "--athena-source-mode",
        type=str,
        choices=["keep", "wipe"],
        help="Mode for the Athena source directory. This is useful for debugging.",
    )

    p.add_argument(
        "--keep-athena",
        "-k",
        action="store_true",
        help="Keep the Athena source directory",
    )

    p.add_argument(
        "--wipe-athena",
        "-w",
        action="store_true",
        help="Wipe the Athena source directory",
    )

    p.add_argument(
        "--jobs",
        "-j",
        type=int,
        default=multiprocessing.cpu_count(),
        help="Number of jobs to use for the build",
    )

    p.add_argument(
        "--jobs-athena",
        type=int,
        help="Number of jobs to use for the Athena build",
    )

    p.add_argument(
        "--add-package-filters",
        "-F",
        action="append",
        help="Additional package filters files to include",
    )

    p.add_argument(
        "--force",
        action="store_true",
        help="Force overwriting existing build environment",
    )

    p.add_argument(
        "--ignore",
        action="store_true",
        help="Ignore existing build environment and use anyway",
    )

    return p


class AthenaSourceMode(enum.Enum):
    KEEP = "keep"
    WIPE = "wipe"


@dataclasses.dataclass
class Arguments:
    acts_repo: str
    acts_ref: str
    acts_source: Path

    athena_repo: str
    athena_ref: str
    athena_release: str
    athena_source: Path

    athena_source_mode: AthenaSourceMode
    no_athena_acts_canary: bool

    geomodel_repo: str
    geomodel_ref: str
    geomodel_source: Path
    build_geomodel: bool

    jobs: int
    jobs_athena: int

    interactive: bool

    directory: Path

    force: bool
    ignore: bool

    add_package_filters: List[Path]

    add_compile_commands: bool


def process_arguments(args: argparse.Namespace) -> Arguments:

    # Resolve all paths
    args.acts_source = args.acts_source.resolve()
    args.athena_source = args.athena_source.resolve()
    args.geomodel_source = args.geomodel_source.resolve()

    if args.jobs_athena is None:
        args.jobs_athena = args.jobs

    if args.directory is None:
        args.directory = make_tmp_dir()
        print("No directory specified, using temporary directory at", args.directory)
    else:
        args.directory = args.directory.resolve()

    if args.athena_source_mode is None:
        if args.keep_athena and args.wipe_athena:
            raise ValueError("Cannot specify both keep and wipe")
        elif args.keep_athena:
            args.athena_source_mode = AthenaSourceMode.KEEP
        elif args.wipe_athena:
            args.athena_source_mode = AthenaSourceMode.WIPE

    kwargs = vars(args)
    kwargs.pop("keep_athena")
    kwargs.pop("wipe_athena")

    return Arguments(**kwargs)


def make_tmp_dir() -> Path:
    return Path(f"/tmp/{os.getlogin()}/ath_build")


def print_config_table(args, lcg_platform: str):
    term_width = shutil.get_terminal_size().columns

    # Define the table data
    rows = [
        ("Build directory", str(args.directory)),
        ("LCG platform", lcg_platform),
        ("ACTS repository", args.acts_repo),
        ("ACTS reference", args.acts_ref),
        ("Athena repository", args.athena_repo),
        ("Athena reference", args.athena_ref),
        ("Athena release", args.athena_release),
        ("Use ACTS canary", str(not args.no_athena_acts_canary)),
        ("GeoModel repository", args.geomodel_repo),
        ("GeoModel reference", args.geomodel_ref),
        ("GeoModel source", str(args.geomodel_source)),
        ("Build GeoModel", str(args.build_geomodel)),
        ("Build jobs", str(args.jobs)),
        ("Build jobs (Athena)", str(args.jobs_athena)),
    ]

    # Calculate column widths
    label_width = max(len(row[0]) for row in rows)
    value_width = term_width - label_width - 7  # 7 accounts for spacing and borders

    # Print header
    label = "Build Configuration"
    print("┌" + "─" * (term_width - 2) + "┐")
    print("│ " + label + " " * (term_width - len(label) - 3) + "│")
    print("├" + "─" * (label_width + 2) + "┬" + "─" * (value_width + 2) + "┤")

    # Print rows
    for label, value in rows:
        # Truncate value if too long
        if len(value) > value_width:
            value = value[: value_width - 3] + "..."
        print(f"│ {label:<{label_width}} │ {value:<{value_width}} │")

    # Print footer
    print("└" + "─" * (label_width + 2) + "┴" + "─" * (value_width + 2) + "┘")


@dataclasses.dataclass
class ExternalsVersions:
    acts: str
    geomodel: str


def find_externals_versions(path: Path) -> ExternalsVersions:
    build_script = path / "Projects/Athena/build_externals.sh"

    content = build_script.read_text()
    geomodel = re.search(
        r"-DATLAS_GEOMODEL_SOURCE=\"URL;[^ ]*/archive/([^/]+)/GeoModel-\1\.tar\.",
        content,
    )

    assert geomodel is not None, "Could not find GeoModel version in build script"

    acts = re.search(r"-DATLAS_ACTS_SOURCE=\"URL;[^ ]*/tags/([^/]+)\.tar\.", content)

    assert acts is not None, "Could not find ACTS version in build script"

    return ExternalsVersions(acts=acts.group(1), geomodel=geomodel.group(1))


def assemble_athena_source(path: Path, ref: str, rel: str, lcg_platform: str):
    assemble_athena_source_exe = Path(__file__).parent / "assemble_athena_source.sh"
    assert (
        assemble_athena_source_exe.exists()
    ), f"assemble_athena_source.sh not found at {assemble_athena_source_exe}"

    subprocess.run(
        [assemble_athena_source_exe],
        env={
            **os.environ,
            "ATHENA_SOURCE": str(path),
            "ATHENA_REF": ref,
            "ATHENA_RELEASE": rel,
            "LCG_PLATFORM": lcg_platform,
            "NIGHTLY_COMMIT": find_nightly_commit(path, ref, rel, lcg_platform),
        },
        cwd=path,
        check=True,
    )


def find_nightly_commit(path: Path, ref: str, rel: str, lcg_platform: str) -> str:
    find_nightly_commit_exe = Path(__file__).parent / "find_nightly_commit.sh"
    assert (
        find_nightly_commit_exe.exists()
    ), f"find_nightly_commit.sh not found at {find_nightly_commit_exe}"
    output = subprocess.run(
        f"source {find_nightly_commit_exe} &> /dev/null  && echo $NIGHTLY_COMMIT",
        shell=True,
        env={
            **os.environ,
            "ATHENA_SOURCE": str(path),
            "ATHENA_REF": ref,
            "ATHENA_RELEASE": rel,
            "LCG_PLATFORM": lcg_platform,
        },
        cwd=path,
        check=True,
        capture_output=True,
        encoding="utf-8",
    )
    return output.stdout.strip()


def prepare_athena_source(
    path: Path,
    url: str,
    ref: str,
    rel: str,
    lcg_platform: str,
    mode: AthenaSourceMode,
    canary: bool,
):
    if path.exists() and mode == AthenaSourceMode.KEEP:
        print(
            "Keeping Athena source already existing at",
            path,
        )
        return

    if not path.exists():
        subprocess.run(["git", "clone", url, "-b", ref, path], check=True)
    else:
        subprocess.run(["git", "reset", "--hard", "HEAD"], cwd=path, check=True)
        subprocess.run(["git", "checkout", ref], cwd=path, check=True)
        subprocess.run(["git", "pull"], cwd=path, check=True)

    if canary:
        assemble_athena_source(path, ref, rel, lcg_platform)
    else:
        nightly = find_nightly_commit(path, ref, rel, lcg_platform)
        print("Checking out Athena nightly", nightly)
        subprocess.run(["git", "checkout", nightly], cwd=path, check=True)


def main():
    parser = get_arg_parser()
    args = process_arguments(parser.parse_args())

    if "ATLAS_LOCAL_ROOT_BASE" not in os.environ:
        raise RuntimeError("ATLAS_LOCAL_ROOT_BASE not set.")

    if platform.machine() == "x86_64":
        lcg_platform = "x86_64-el9-gcc13-opt"
    elif platform.machine() in ["aarch64", "arm64"]:
        lcg_platform = "aarch64-el9-gcc13-opt"
    else:
        raise ValueError(f"Unsupported platform {platform.machine()}")
    os.environ["LCG_PLATFORM"] = lcg_platform

    prepare_athena_source(
        path=args.athena_source,
        url=args.athena_repo,
        ref=args.athena_ref,
        rel=args.athena_release,
        lcg_platform=lcg_platform,
        mode=args.athena_source_mode,
        canary=not args.no_athena_acts_canary,
    )

    versions = find_externals_versions(args.athena_source)

    acts_ref = args.acts_ref
    geomodel_ref = args.geomodel_ref

    if acts_ref == "auto":
        acts_ref = versions.acts
    if geomodel_ref == "auto":
        geomodel_ref = versions.geomodel

    print_config_table(args, lcg_platform)

    os.environ["ATHENA_SOURCE"] = str(args.athena_source)
    os.environ["ACTS_SOURCE"] = str(args.acts_source)
    os.environ["ATHENA_GIT_REPO"] = args.athena_repo
    os.environ["ATHENA_RELEASE"] = args.athena_release
    os.environ["ATHENA_REF"] = args.athena_ref
    os.environ["ACTS_GIT_REPO"] = args.acts_repo
    os.environ["ACTS_REF"] = acts_ref
    os.environ["GEOMODEL_GIT_REPO"] = args.geomodel_repo
    os.environ["GEOMODEL_REF"] = geomodel_ref
    os.environ["GEOMODEL_SOURCE"] = str(args.geomodel_source)

    os.environ["BUILD_JOBS"] = str(args.jobs)
    os.environ["BUILD_JOBS_ATHENA"] = str(args.jobs_athena)

    if args.interactive:
        os.environ["INTERACTIVE"] = "1"

    if args.athena_source_mode is not None:
        os.environ["ATHENA_SOURCE_MODE"] = args.athena_source_mode.value

    if args.build_geomodel:
        os.environ["BUILD_GEOMODEL"] = "1"

    if args.directory.exists() and not args.ignore:
        if not args.force:
            raise RuntimeError(
                "Build environment directory already exists. Rerun with --force to recreate or --ignore to ignore."
            )
        else:
            shutil.rmtree(args.directory)
    args.directory.mkdir(parents=True, exist_ok=True)

    # WTODO: Refactor
    package_filters_dst = args.directory / "package_filters.txt"
    if package_filters_dst.exists():
        package_filters_dst.unlink()

    package_filters_hack = Path(__file__).parent / "package_filters_hack.txt"
    assert (
        package_filters_hack.exists()
    ), f"package_filters_hack.txt not found at {package_filters_hack}"

    with package_filters_dst.open("w") as fh:
        fh.write(package_filters_hack.read_text())

        for package_filters in args.add_package_filters or []:
            package_filters = Path(package_filters)
            if not package_filters.exists():
                raise ValueError(f"package_filters not found at {package_filters}")
            fh.write(f"\n\n# From {package_filters.resolve()}")
            fh.write("\n")
            fh.write(package_filters.read_text())

        fh.write("\n\n# Exclude other packages\n- .*")

    print(package_filters_dst.read_text())

    build_script = Path(__file__).parent / "athena_build.sh"
    assert build_script.exists(), f"athena_build.sh not found at {build_script}"
    sys.stdout.flush()  # need to flush before running command

    subprocess.run([build_script], check=True, cwd=args.directory)

    if args.add_compile_commands:
        out = []
        for f in [
            "acts-build/compile_commands.json",
            "athena-build/compile_commands.json",
        ]:
            f = args.directory / f
            if not f.exists():
                raise ValueError(f"compile_commands.json not found at {f}")
            with f.open() as fh:
                out += json.load(fh)
        with (args.directory / "compile_commands.json").open("w") as fh:
            json.dump(out, fh, indent=2)

    if not args.interactive:
        with tempfile.NamedTemporaryFile("w") as f:
            f.write(
                """
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    asetup --restore
    cmake --build athena-build
    """.strip()
            )
            f.flush()

            subprocess.run(
                ["bash", f.name],
                check=True,
                cwd=args.directory,
            )
    else:
        with tempfile.NamedTemporaryFile("w") as f:
            f.write(
                """
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    asetup --restore
    exec bash -i
    """.strip()
            )
            f.flush()

            subprocess.run(
                [os.environ["SHELL"], f.name],
                cwd=args.directory,
            )


if "__main__" == __name__:
    main()
