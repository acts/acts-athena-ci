#!/bin/bash
set -e

if [ ! -z "$PATCH_URLS" ]; then
    echo "$PATCH_URLS" | sed -n 1'p' | tr ',' '\n' | while read url; do
        echo "---- Applying ${url} ----"
        curl "${url}" 2> /dev/null | git am -3
        git --no-pager log -1 $sha
        echo "----------------"
    done
fi
