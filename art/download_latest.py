#!/usr/bin/env python3
import sys
import os
import argparse

from sshfs import SSHFileSystem


p = argparse.ArgumentParser()
p.add_argument("--dst")
p.add_argument("test_name")
args = p.parse_args()


fs = SSHFileSystem("lxplus.cern.ch", username=os.environ["EOS_USER_NAME"], password=os.environ["EOS_USER_PWD"])
base_path = os.environ["EOS_BASE_PATH"]

folders = [ d["name"] for d in fs.ls(f"{base_path}outputs/{args.test_name}", detail=True) if d["type"] == "directory"]
folders.sort(reverse=True)
if len(folders) == 0:
    print("No latest folder available")
    sys.exit(0)
latest_folder = folders[0]
print("Latest folder is", latest_folder)

fs.get(latest_folder, args.dst, recursive=True)
