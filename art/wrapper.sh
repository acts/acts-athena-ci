#!/bin/bash
set -u
set -e

run() { 
    set -x
    "$@" 
    { set +x; } 2> /dev/null
}
script=${1}

UPLOAD_RESULTS=${UPLOAD_RESULTS:-}

if [[ $script =~ .*/(.+)\.sh$  ]]; then
    export test_name="${BASH_REMATCH[1]}"
else
    echo "Unable to extract test name"
    exit 1
fi

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}"   )" &> /dev/null && pwd   )

tmp_dir=$(mktemp -d)
export inputs_dir=${tmp_dir}/inputs
mkdir "${inputs_dir}"
export lastref_base_dir=${tmp_dir}/lastref
mkdir "${lastref_base_dir}"

timestamp=$(date +%Y-%m-%dT%H-%M-%S)
export timestamp

export PATH="$script_dir:$PATH"

if [ -n "${SOURCE_SHA:-}" ]; then
    acts_source_sha=${SOURCE_SHA}
else
    # no explicit source commit, use current main
    acts_source_sha=$(curl -s https://api.github.com/repos/acts-project/acts/commits | jq -r .[0].sha)
fi

function cleanup {
    echo "Running cleanup"
    # make extra sure we don't mess up
    if [ -n "${tmp_dir}" ]; then
        run rm -rf "${tmp_dir}"
    fi

    # remove symlink if created
    if [ -L "lastref_dir" ]; then
        run rm lastref_dir
    fi
}

trap cleanup EXIT

export SSH_CONN=${EOS_USER_NAME}@lxplus.cern.ch

function download_file {
    set -u
    set -e
    local src="$1"
    local dst="$2"
    echo "Downloading ${EOS_BASE_PATH}/${src} to ${dst}"
    rsync "${SSH_CONN}":"${EOS_BASE_PATH}/${src}" "${dst}"
}
export -f download_file

function download_latest {
    set -u
    set -e
    local base_path="${EOS_BASE_PATH}/outputs/${test_name}"
    local latest
    latest=$(ssh "${SSH_CONN}" "(ls ${base_path} 2>/dev/null | grep -E '[0-9]{4}-[0-9]{2}-[0-9]{2}T.+' | sort | tail -n1) || true")
    if [ -z "$latest" ]; then
        echo "No latest folder found"
    fi
    local full_path="${base_path}/${latest}"
    local output_path="${lastref_base_dir}/${latest}"
    mkdir "${output_path}"
    run rsync -rl "${SSH_CONN}":"${full_path}/" "${output_path}"
    ln -s "${output_path}" lastref_dir
    echo "${output_path}"
}
export -f download_latest

# ART scripts don't expect to run with -e
set +e
${script}
script_ec=$?
set -e

if [ $script_ec -ne 0 ]; then
    echo "Script failed with exit code $script_ec"
fi

cp "${script}" .

if [ -n "${UPLOAD_RESULTS}" ]; then
    echo "Uploading results"

		echo "<h1>ART run</h1>" > README.html

		echo "<br>" >> README.html

		if [ -n "${ATHENA_RELEASE:-}" ]; then
				echo "Athena release: <code>${ATHENA_RELEASE}</code>" >> README.html
				if [ -n "${Athena_DIR:-}" ]; then
						echo "<br><code>${Athena_DIR}</code>" >> README.html
				fi
		else
				echo "Athena release: No info on Athena release" >> README.html
		fi

		if [ -n "${LCG_PLATFORM:-}" ];then
				echo "<br>" >> README.html
				echo "LCG platform: <code>${LCG_PLATFORM}</code>" >> README.html
		fi

		echo "<br>" >> README.html
		echo "${acts_source_sha}" > acts_source_sha.txt
		echo "This ACTS commit: <a href=\"https://github.com/acts-project/acts/commit/${acts_source_sha}\">${acts_source_sha:0:7}</a>" >> README.html
		echo "<br>" >> README.html

		last_file="lastref_dir/acts_source_sha.txt"
		if [ -f "${last_file}" ];then
				last_source_sha=$(cat "${last_file}")
				echo "Last ACTS commit: <a href=\"https://github.com/acts-project/acts/commit/${last_source_sha}\">${last_source_sha:0:7}</a>" >> README.html
				echo "<br>" >> README.html

				if [ "${acts_source_sha}" != "${last_source_sha}" ]; then
						compare_url="https://github.com/acts-project/acts/compare/${last_source_sha}...${acts_source_sha}"
						echo "<a href=\"${compare_url}\">Changes between <code>${last_source_sha:0:7}</code> and <code>${acts_source_sha:0:7}</code></a>" >> README.html
						echo "<br>" >> README.html
				fi
		fi

		target="${EOS_BASE_PATH}/outputs/${test_name}"
		ssh "${SSH_CONN}" "[ -d ${target} ] || mkdir ${target}"
		ssh "${SSH_CONN}" "[ -d ${target}/pulls ] || mkdir ${target}/pulls"

    rsync_opts="-rul"

		if [ -z "${SOURCE_PULL:-}" ]; then
				target="${target}/${timestamp}"
		else
				target="${target}/pulls/${SOURCE_PULL}"
        rsync_opts+=" --delete"

				echo "Source Pull Request: <a href=\"https://github.com/acts-project/acts/pull/${SOURCE_PULL}\">#${SOURCE_PULL}</a>" >> README.html
				echo "<br>" >> README.html
		fi

		ssh "${SSH_CONN}" "[ -d ${target} ] || mkdir ${target}"
		echo "Uploading results to ${target}"
		rsync ${rsync_opts} . "${SSH_CONN}":"${target}/"

		if [ -z "${SOURCE_PULL:-}" ]; then
				echo "PUBLIC_URL: https://acts-athena-outputs.web.cern.ch/${test_name}/${timestamp}/"
		else
				echo "PUBLIC_URL: https://acts-athena-outputs.web.cern.ch/${test_name}/pulls/${SOURCE_PULL}/"
		fi

else
  echo "Skipping uploading of results"
fi

exit $script_ec
