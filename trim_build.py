#!/usr/bin/env python3

import argparse
import os
from pathlib import Path


def main():
    p = argparse.ArgumentParser(description="Trim the build folder")
    p.add_argument("path", type=Path, help="Path to the build folder")
    p.add_argument("--dry-run", "-S", action="store_true", help="Do not delete files")
    p.add_argument(
        "--verbose", "-v", action="store_true", help="Print files that would be deleted"
    )
    p.add_argument(
        "--size-threshold", type=float, default=0.1, help="Size threshold in MB"
    )

    args = p.parse_args()

    args.path = args.path.resolve()
    if not args.path.exists():
        raise ValueError(f"Path {args.path} does not exist")

    total_deleted = 0

    def delete(path: Path):
        nonlocal total_deleted
        total_deleted += os.stat(path).st_size
        if args.dry_run:
            if args.verbose:
                print(f"Would delete {path.relative_to(args.path)}")
        else:
            path.unlink()

    def handle_test_exe(path: Path):
        stat = os.stat(path)
        size_mb = stat.st_size / 1024 / 1024
        if size_mb > args.size_threshold:
            if args.verbose:
                print(
                    f"File {path.relative_to(args.path)} is {size_mb:.2f} MB (over threshold: {args.size_threshold} MB)"
                )
            delete(path)
        else:
            if args.verbose:
                print(
                    f"File {path.relative_to(args.path)} is {size_mb:.2f} MB (under threshold: {args.size_threshold} MB)"
                )

    for root, _, files in os.walk(args.path):
        root = Path(root)
        for file in files:
            full_file = root / file
            if file.endswith(".o") or file.endswith(".so.dbg"):
                delete(full_file)
            if root.name == "test-bin" and file.endswith(".exe"):
                handle_test_exe(full_file)

    print(f"Deleted {total_deleted / 1024 / 1024:.2f} MB")


if __name__ == "__main__":
    main()
