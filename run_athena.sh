#!/bin/bash

tmp=/tmp/$(whoami)/ath_build

pushd $tmp
source athena-install/setup.sh
export LD_LIBRARY_PATH=$PWD/acts-install/lib64:$LD_LIBRARY_PATH
echo "Setup ready, dropping into shell"
bash
popd
