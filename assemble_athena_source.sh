#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}"  )" &> /dev/null && pwd  )

echo "Latest nightly for '${ATHENA_RELEASE}' was created at ${NIGHTLY_DATE}"
echo "Last commit before nightly determined to be $NIGHTLY_COMMIT"
echo "---- Now at ----"
git --no-pager log -1 $NIGHTLY_COMMIT
echo "----------------"

git checkout $NIGHTLY_COMMIT

if [ -z "$NO_CANARY" ]; then
    echo "Merging MRs labeled 'nightly:ACTS'"
    ./Build/AtlasBuildScripts/gitlab_merge.sh -b ${ATHENA_REF} -l nightly:ACTS
fi

$SCRIPT_DIR/apply_patches.sh
$SCRIPT_DIR/apply_extra_patches.sh
