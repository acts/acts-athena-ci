#!/bin/bash
function group {
    n=${#1}
    s=$(printf "=%${n}s" | tr " " "=")
    echo ""
    echo $s
    echo "${@}"
    echo $s
    echo ""
}

export group
    
function run() { 
    set -x
    "$@" 
    { set +x;  } 2> /dev/null
}

export run
