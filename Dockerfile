# @TODO: Update to real alma9-atlasos image when ready
FROM gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos-dev:1.0.0

USER root

RUN dnf install -y jq libnsl && dnf -y clean all
