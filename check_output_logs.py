#!/usr/bin/env python3
from __future__ import print_function
import os
import argparse
import re

p = argparse.ArgumentParser()
p.add_argument("dir", help="Argument to search logfiles for")
p.add_argument("--acts", help="ACTS installation directory", required=True)
p.add_argument("--nightly-mode", help="Which nightly mode is being run", required=True)
args = p.parse_args()

names = [
    "log.RAWtoALL",
    "run.log",
]

do_version_check = args.nightly_mode == "hack"

if do_version_check:
    with open(os.path.join(args.acts, "cmake/Acts/ActsConfig.cmake")) as fh:
        config = fh.read()
    install_commit = re.search(r"set\(Acts_COMMIT_HASH \"(.*)\"\)", config).group(1)
    with open(os.path.join(args.acts, "cmake/Acts/ActsConfigVersion.cmake")) as fh:
        config_version = fh.read()
    install_version = re.search(r"set\(PACKAGE_VERSION \"(\d+\.\d+\.\d+)\"\)", config_version).group(1)

for name in names:
    file = os.path.join(args.dir, name)
    if not os.path.exists(file):
        continue

    with open(file) as fh:
        for line in fh:
            if do_version_check:
                m = re.match(r".*ACTS version is: v(\d+\.\d+\.\d+) \[(.*)\]", line)
                if m is not None:
                    version, commit = m.groups()
                    if version != install_version:
                        raise ValueError("Installed version not loaded: {} vs {}".format(version, install_version))
                    if commit != install_commit:
                        raise ValueError("Installed version not loaded: {} vs {}".format(commit, install_commit))

            if "ERROR Failure running application" in line:
                raise ValueError("Error running application")
            if "ERROR Failed to initialize AppMgr" in line:
                raise ValueError("Error initializing AppMgr")

